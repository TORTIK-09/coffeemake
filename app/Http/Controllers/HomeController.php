<?php

namespace App\Http\Controllers;
use \Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function update($id, User $request)
	{
        $user = User::find($id);
        $user->update($id, $request->all());
        $user->save();
        return redirect('users');
        
	}
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    
}
