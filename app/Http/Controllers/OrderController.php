<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderQuery;
class OrderController extends Controller
{
    public function create(){
        return view('buy.index');
    }

    public function order(Request $request)
    {
        $data = $request -> validate([
            'name'     => ['required', 'string', 'max:255'],
            'email'    => ['required', 'string', 'email', 'max:255'],
            'phone'  => ['required', 'int', 'min:11'],
            'order'  => ['required', 'string', 'max:255'],  
       ] );

        Mail::to('nesterenko.k.2004trtk@gmail.com')->send(new OrderQuery($data));
        return back()->with('success','Спасибо за ваш заказ!');
    }
}