@extends('layouts.app')
<style>
.background .shape{
    height: 200px;
    width: 200px;
    position: absolute;
    border-radius: 50%;
}
.shape:first-child{
    background: linear-gradient(
        #1845ad,
        #23a2f6
    );
    left: -50px;
    top: -50px;
}
.shape:last-child{
    background: linear-gradient(
        to right,
        #ff512f,
        #f09819
    );
    right: 0px;
    bottom: -80px;
}
</style>
<div  class="background">
        <div class="shape"></div>
        <div class="shape"></div>
    </div>
@section('content')

<?php
use Illuminate\Support\Facades\Auth;
?>

<div  class="container" style="opacity: 0.9;">

    <div class="row justify-content-center"data-aos="zoom-in-up" data-aos-duration="1000" data-aos-delay="600" >
        <div class="col-md-8">
            
            <div class="card">
                
                <div class="card-header" style="font-weight: 500;">{{ __('Личный Кабинет') }}</div>
                
                <div style="background-image:url({{asset('images/card-image.jpg')}}); background-size:100%;" class="card-body" style="font-weight: 500;">
              

    <div class="row justify-content-center align-items-center">
      <div class="col col-lg-12 mb-4 mb-lg-0">
        <div class="card mb-3" style="border-radius: .5rem;">
          <div class="row g-0">
            <div class="col-md-4 gradient-custom text-center text-white"
              style="background-image: url({{asset('images/profile_back.jfif')}}); background-size:100%; border:solid; border-right:0.5em">
              <img src="{{asset('images/profile-transformed.webp')}}"
                alt="Avatar" class="img-fluid my-5" style="width: 80px;" />
              <h5>{{ Auth::user()->name }}</h5>
              <hr class="mt-0 mb-4">
              <a class=" btn btn-outline-warning" href="{{ route('edit') }}" role="button">
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
  <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
  <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
</svg> Изменить
</a>
              
            </div>
            <div style="background-image:url({{asset('images/profile_back_rev.webp')}}); background-size:100%; border:solid; border-color:white;" class="text-white col-md-8">
              <div class="card-body p-4">
                <h6>Информация</h6>
                <hr class="mt-0 mb-4">
                <div class="row pt-1">
                  <div class="col-6 mb-3">
                    <h6>Почта</h6>
                    <hr class="mt-0 mb-4">
                    <p class="text-white ">{{ Auth::user()->email }}</p>
                  </div>
                  <div class="col-6 mb-3">
                    <h6>Дата рождения</h6>
                    <hr class="mt-0 mb-4">
                    <p class="text-white">{{ Auth::user()->date }}</p>
                  </div>
                </div>
                <h6>Пользователь</h6>
                <hr class="mt-0 mb-4">
                <div class="row pt-1">
                  <div class="col-6 mb-3">
                    <h6>Псевдоним</h6>
                    <hr class="mt-0 mb-4">
                    <p class="text-white">{{ Auth::user()->name }}</p>
                  </div>
                  <div class="col-6 mb-3">
                    <h6>Полное имя</h6>
                    <hr class="mt-0 mb-4">
                    <p class="text-white">{{ Auth::user()->fullname }}</p>
                  </div>
                </div>
                <div class="d-flex justify-content-start">
                  <a href="#!"><i class="fab fa-facebook-f fa-lg me-3"></i></a>
                  <a href="#!"><i class="fab fa-twitter fa-lg me-3"></i></a>
                  <a href="#!"><i class="fab fa-instagram fa-lg"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>



            
                <script>
                     if ($(window).width() < 1025) {
                        document.getElementById("sf").style.display = "none";
}
                </script>
                <br>
                <a style="margin-left: 0%;color=black; font-weight: 600;border-style:solid;width:220px; " class="nav-item dropdown navbarDropdown btn btn-dark" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Выйти') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
            </div>
        </div>
        
    </div>
</div>
@endsection
