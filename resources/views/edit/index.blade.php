
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta
	name="viewport"
	content="
		width=device-width,
		initial-scale=1.0,
		minimal-scale=1.0,
		maximal-scale=1.0,
		user-scalable=no
	"
>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>CoffeeMake</title>

    <!-- Scripts -->
    
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <script src="{{ asset('js/app.js') }}" defer></script>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/logohead.webp')}}">
</head>
<style>
     body{
   /* background: linear-gradient(to bottom,#f9fbf7, #C9D8FF); */
   height:100%;
   display:table;
   width:100%;
   text-align:center;
   background-attachment: fixed;
   background-repeat:no-repeat;
   background-size:100%;
   background-color: #080710;
}
    .background .shape{
    height: 200px;
    width: 200px;
    position: absolute;
    border-radius: 50%;
}
.shape:first-child{
    background: linear-gradient(
        #1845ad,
        #23a2f6
    );
    left: -80px;
    top: -80px;
}
.shape:last-child{
    background: linear-gradient(
        to right,
        #ff512f,
        #f09819
    );
    right: -30px;
    bottom: -80px;
}
</style>
<div class="background">
        <div class="shape"></div>
        <div class="shape"></div>
    </div>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm " >
            <div class="container ">
                <a class="octocenter navbar-brand " href="{{ url('/') }}">
                    CoffeeMake
                </a>
                <button class="navbar-toggler drop-down__button" type="button"  data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon "></span>
                    
                </button>
                <div class="collapse navbar-collapse drop-down " id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                   
                    </ul>
                    
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Логин') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Регистрация') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link" href="/home" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>
                               
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
<?php

use Hamcrest\Text\StringContains;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
setlocale(LC_ALL, 'russian');
$id = intval(Auth::user()->id);
$email= (Auth::User()->email);
$name= (Auth::User()->name);
$password = (Auth::User()->password);
$fullname = (Auth::User()-> fullname);
$con = mysqli_connect("DataBase_Host", "Login", "Password", "Database");
if (isset($_GET['edit']))
{
    $email = mysqli_real_escape_string($con, $_GET['email']);
    if (mysqli_query($con,"UPDATE users SET email='$email' WHERE id = '$id'; ") )
    {
        header("Refresh: 3; http://diplomtortik.byethost8.com/public/home");
        echo 'Операция выполнена успешно!';
    }
    else
    {
        header('Refresh: 10');
        echo 'Ошибка. Изменения не были сохранены. Страница обновится через 10 секунд.';
    }
}
if (isset($_GET['edit_name']))
{
    $name = mysqli_real_escape_string($con, $_GET['name']);
    if (mysqli_query($con,"UPDATE users SET name='$name' WHERE id = '$id'; ") )
    {
        header("Refresh: 3; http://diplomtortik.byethost8.com/public/home");
        echo 'Операция выполнена успешно!';
    }
    else
    {
        header('Refresh: 10');
        echo 'Ошибка. Изменения не были сохранены. Страница обновится через 10 секунд.';
    }
}
if (isset($_GET['edit_password']))
{
    $password = mysqli_real_escape_string($con, Hash::make($_GET['password']));

    if (mysqli_query($con,"UPDATE users SET password='$password' WHERE id = '$id';"))
    {
        header("Refresh: 3; http://diplomtortik.byethost8.com/public/home");
        echo 'Операция выполнена успешно!';
    }
    else
    {
        header('Refresh: 10');
        echo 'Ошибка. Изменения не были сохранены. Страница обновится через 10 секунд.';
    }
}
if (isset($_GET['edit_fullname']))
{
    $fullname = mysqli_real_escape_string($con, ($_GET['fullname']));

    if (mysqli_query($con,"UPDATE users SET fullname='$fullname' WHERE id = '$id';"))
    {
        header("Refresh: 3; http://diplomtortik.byethost8.com/public/home");
        echo 'Операция выполнена успешно!';
    }
    else
    {
        header('Refresh: 10');
        echo 'Ошибка. Изменения не были сохранены. Страница обновится через 10 секунд.';
    }
}
?>


<div class="container" style="opacity: 0.9;">

    <div class="row justify-content-center">
        <div class="col-md-8">
            
            <div class="card" >
                
                <div class="card-header" style="color:black;font-weight:500;">{{ __('Редактирование пользователя') }}</div>

<div class="text-center border form-control" id="edit">
    <form method="get">

        <h5 class="text-center">Действующий Псевдоним: <br> <div style="color:brown;"><?php echo $name; ?><div></h5>
        <br>
        <h5 class="text-center">Действующее Ф.И.О: <br> <div style="color:brown;"><?php echo $fullname ?></div></h5>
        <br>
        <h5 class="text-center">Действующий Email: <br> <div style="color:brown;"><?php echo $email ?></div></h5>
        
        <br>
        <br>

        <label class="text-center " for="email_input">Новый Email(можете старый)</label>
        <input class="sf text-center form-control " style="margin-left:30%;width:300px;" name="email" type="text" id="email_input" placeholder="Введите новый email" value="<?php echo htmlspecialchars($email); ?>" required >
<br>
        <button type="submit" name="edit" class="btn btn-dark" style="margin-left: -1%;">Сохранить</button>
        <br><br>

    </form>
</div>
<div class="text-center border form-control" id="edit_name">
    <form method="get">
        <label style="margin-left:-2%;" for="name_input">Новый Псевдоним</label>
        <input class="text-center form-control" style="margin-left:30%;width:300px;" name="name" type="text" id="name_input" placeholder="Введите новое имя" value="<?php echo htmlspecialchars($name); ?>">
<br>

        <button type="submit" name="edit_name" class="btn btn-dark" style="margin-left: -1%;">Сохранить</button>
        <br><br>

    </form>
</div>
<div class="text-center border form-control" id="edit_fullname">
    <form method="get">
        <label style="margin-left:-2%;" for="fullname_input">Новое Ф.И.О.</label>
        <input class="text-center form-control" style="margin-left:30%;width:300px;" name="fullname" type="text" id="fullname_input" placeholder="Введите новое Ф.И.О." value="<?php echo htmlspecialchars($fullname); ?>">
<br>

        <button type="submit" name="edit_fullname" class="btn btn-dark" style="margin-left: -1%;">Сохранить</button>
        <br><br>

    </form>
</div>
<div class="text-center border form-control" id="edit_password">
    <form method="get">
        <label  style="margin-left:-2%;" for="password_input">Пароль</label>
        <input class="text-center form-control " style="margin-left:30%;width:300px;" name="password" type="password" id="password_input" placeholder="<?php echo htmlspecialchars($password); ?>">

        <br>

        <button type="submit" name="edit_password" class="btn btn-dark" style="margin-left: -1%;">Сохранить</button>
        <br><br><br>
        <a href="{{ route('login') }}" class="" style="margin-left:-1%;">Назад</a>

    </form>
</div>
</div>
        </div>
    </div>
</div>
</main>
<script>
        
        </script>
        <script>
                AOS.init({
                    easing: 'ease-out-back',
                    duration: 1000
                });
            </script>
</body>
</html>