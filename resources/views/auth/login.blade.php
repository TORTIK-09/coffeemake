@extends('layouts.app')
<style>
    body{
    background-color: #080710;
}

.background .shape{
    height: 200px;
    width: 200px;
    position: absolute;
    border-radius: 50%;
}
.shape:first-child{
    background: linear-gradient(
        #1845ad,
        #23a2f6
    );
    left: -80px;
    top: -80px;
}
.shape:last-child{
    background: linear-gradient(
        to right,
        #ff512f,
        #f09819
    );
    right: -30px;
    bottom: -80px;
}
form{
    
    height: 320px;
    width: 500px;
    background-color: rgba(255,255,255,0.13);
    position: absolute;
    transform: translate(-50%,-50%);
    margin-top:50%;
    top: 50%;
    left: 50%;
    bottom:50%;
    right:50%;
    border-radius: 10px;
    backdrop-filter: blur(10px);
    border: 2px solid rgba(255,255,255,0.1);
    box-shadow: 0 0 40px rgba(8,7,16,0.6);
    padding: 50px 35px;
}
form *{
    font-family: 'Poppins',sans-serif;
    color: #ffffff;
    letter-spacing: 0.5px;
    outline: none;
    border: none;
}
form h3{
    font-size: 32px;
    font-weight: 500;
    line-height: 42px;
    text-align: center;
}

label{
    display: block;
    margin-top: 30px;
    font-size: 16px;
    font-weight: 500;
}
input{
    display: block;
    height: 50px;
    width: 100%;
    background-color: rgba(255,255,255,0.07);
    border-radius: 3px;
    padding: 0 10px;
    margin-top: 8px;
    font-size: 14px;
    font-weight: 300;
}
::placeholder{
    color: #e5e5e5;
}
button{
    margin-top: 50px;
    width: 100%;
    background-color: #ffffff;
    color: #080710;
    padding: 15px 0;
    font-size: 18px;
    font-weight: 600;
    border-radius: 5px;
    cursor: pointer;
}
.social{
  margin-top: 30px;
  display: flex;
}
.social div{
  background: red;
  width: 150px;
  border-radius: 3px;
  padding: 5px 10px 10px 5px;
  background-color: rgba(255,255,255,0.27);
  color: #eaf0fb;
  text-align: center;
}
.social div:hover{
  background-color: rgba(255,255,255,0.47);
}
.social .fb{
  margin-left: 25px;
}
.social i{
  margin-right: 4px;
}

@media screen and (max-width:2560px) {
    nav{
        right:0%;
        width:2560px;
        text-align:center;
    size:auto;
}
}

@media screen and (max-width:1920px) {
    nav{
        right:0%;
        width:1920px;
        text-align:center;
    size:auto;
}
form{
    margin-top:25%;
    right:50%;
    margin-right:50%;
    margin-left:0%;
}
}

@media screen and (max-width:1440px) {
    nav{
        right:0%;
        width:1440px;
        text-align:center;
    size:auto;
}
form{
    margin-top:25%;
    margin-left:0%;
}
}

@media screen and (max-width:1024px) {
    nav{
        right:0%;
        width:1024px;
        text-align:center;
    size:auto;
}
    form{
    margin-top:50%;
    margin-left:0%;
}
div{
    width:auto;
    height:auto;
    size:auto;
}
}

@media screen and (max-width:768px) {
    nav{
        right:0%;
        width:768px;
        text-align:center;
    size:auto;
}
    form{
    margin-top:50%;
}
div{
    width:auto;
    height:auto;
    size:auto;
}
}
@media screen and (max-width:425px) {
    form{
        width:400px;
        height:375px;
    }
.navbar-brand{
    left:50%;
    margin-left:38%;
}
    nav{
        right:0%;
        width:425px;
        text-align:center;
    size:auto;
}
    div{
    width:auto;
    height:auto;
    size:auto;
}
}

@media screen and (max-width:375px) {
    form{
        width:350px;
        height:385px;
    }
    .navbar-brand{
        left:47%;
        margin-left:37%;
    }
    nav{
    width:375px;
    size:auto;
}
div{
    width:auto;
    height:auto;
    size:auto;
}
}
@media screen and (max-width:320px) {
    form{
        margin-top:60%;
        width:300px;
        height:385px;
    }
    .navbar-brand{
        left:47%;
        margin-left:37%;
    }
    nav{
    width:320px;
    size:auto;
}
div{
    width:auto;
    height:auto;
    size:auto;
}
}
</style>



<div class="background">
        <div class="shape"></div>
        <div class="shape"></div>
    </div>

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="" data-aos="zoom-in-up" data-aos-duration="1500" data-aos-delay="600">
              
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <h3 style="margin-top:-8%; color:white;">{{ __('Логин') }}</h3>
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Адрес') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ 'Неверный логин или пароль' }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

  
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Пароль') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required autocomplete="current-password" autofocus>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ 'Неправильный пароль' }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label style="margin-top:1%;" class="form-check-label" for="remember">
                                        {{ __('Запомни меня') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div  class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Войти') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Забыли пароль?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
